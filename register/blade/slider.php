<?php
BladeHelper::createClosureFunctionTag('Slider',function($arr){

    $slider = $arr[0];

    $db_slider = \Skimia\Pictures\Data\Models\Slider\Slider::where('identifier',$slider)->first();

    if(!isset($db_slider))
        return 'le slider "'.$slider.'" est introuvable"';
    return View::make('skimia.pictures::activities.sliders.render_slider',['slider'=>$db_slider,'slides'=>$db_slider->slides])->render();

});