<?php
use Skimia\Backend\Managers\Bridge;

Hook::register('activities.pages_manager.sidenav-item',function(Bridge $bridge){
    $acl = App::make('acl');
    if($acl->can('pictures.sliders')){
        $state = 'pictures_manager.sliders*list';
        $stateParams = [];
    }elseif( $acl->can('pictures.sliders.slides')){
        $state = 'pictures_manager.slides*list';
        $slider = \Skimia\Pictures\Data\Models\Slider\Slider::first(['id']);
        if($slider == null)
            return;
        $stateParams = ['slider_id'=>$slider->id];
    }else{
        return;
    }

    $bridge->items = [
        'sliders'=>[
            'icon'        => 'os-icon-picture-3',
            'name'        => 'Sliders',
            'type'        => 'state',
            'color'        => 'teal lighten-1',
            'state'       => $state,
            'stateParams' => $stateParams
        ]
    ];
},1000);