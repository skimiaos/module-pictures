<?php


/**
 * Crée une icone disponible pour le dashboard de l'os
 *
 *
 */
Route::matched(function($route, $request)
{
    $acl = App::make('acl');
    if($acl->can('pictures.sliders')){
        $state = 'pictures_manager.sliders*list';
        $stateParams = [];
    }elseif( $acl->can('pictures.sliders.slides')){
        $state = 'pictures_manager.slides*list';
        $slider = \Skimia\Pictures\Data\Models\Slider\Slider::first(['id']);
        if($slider == null)
            return;
        $stateParams = ['slider_id'=>$slider->id];
    }else{
        return;
    }
    Tiles::makeFromState('sliders',$state,$stateParams,'Gestion des Sliders','os-icon-picture-3','teal lighten-1');
    Tiles::makeFromState('sliders.index','pictures_manager',[],'Gestion des Sliders','os-icon-picture-3','teal lighten-1');
});
