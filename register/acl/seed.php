<?php


Event::listen('artisan.os.install.acl',function($manager){
    Eloquent::unguard();
    $manager->attachActions([
        'pictures.sliders',
        'pictures.sliders.edit',
        'pictures.sliders.delete',
        'pictures.sliders.create',
        'pictures.sliders.slides'
    ]);


    $acl = $manager->getAcl();

    $acl->allow('Member', ['pictures.sliders.slides']);

},9999);