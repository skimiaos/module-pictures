<?php

$app = Angular::get(OS_APPLICATION_NAME);


$app->addState('pictures_manager','/pictures-manager',function($params){
    return View::make('skimia.pages::activities.pages-manager.index',$params);
});


\Skimia\Pictures\Data\Forms\Slider\SlidersCRUDForm::register($app,'pictures_manager');
\Skimia\Pictures\Data\Forms\Slider\SlidesCRUDForm::register($app,'pictures_manager');