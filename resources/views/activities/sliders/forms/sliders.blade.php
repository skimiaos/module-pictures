<os-container direction="column" class="layout--page-inner">
    <div class="layout--page-content">
        @FormField('identifier')

        @FormField('description')

        @FormField('data_options')
        <div class="btn-group">
            <a flex class="btn btn-success" ng-click="save(form,$event,1)">Enregistrer et rester</a>
            <a flex class="btn btn-warning" ng-click="save(form,$event,2)">Enregistrer</a>
            <a flex class="btn btn-danger" ng-click="save(form,$event,3)">Annuler</a>
        </div>
    </div>
</os-container>


@Controller
@AddDependency('$rootScope')
//<script>
    $scope.save = function(form,$event,status){
        if(status == 3){
            $rootScope.back();
            return
        }
        @if(isset($urls['edit'] ))

        if(angular.isDefined(form.id) && {{$state == 'edit'?'true':'false'}}){

            $url = replace('{{ $urls['edit'] }}',{':id':form.id});
            $angular_response($scope,$event).post($url,form).success(function(data){


                if(status == 2){
                    $rootScope.back();
                    return
                }
            });
        }

                @endif
                @if(isset($urls['create'] ))
                @if(isset($urls['edit'] )) else @endif
             if({{$state == 'create'?'true':'false'}}){

            $url = '{{ $urls['create'] }}';

            $angular_response($scope,$event).post($url,form).success(function(data){

                if(status == 2){
                    $rootScope.back();
                    return
                }
            });
        }@endif


    };
    //</script>
@EndController