<ul class="slides">
    @foreach($slides as $slide)
    <li>
        <img src="{{ $slide->picture['path'] }}" alt="{{ $slide->picture['description'] }}"> <!-- random image -->
    </li>
    @endforeach
</ul>