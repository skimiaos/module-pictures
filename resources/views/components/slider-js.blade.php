//<script>





    $(function() {
        $('#{{$__id}}').owlCarousel({
            loop: {{ $loop ? 'true' : 'false' }},
            margin:10,
            nav: {{ $nav ? 'true' : 'false' }},
            dots: {{ $dots ? 'true' : 'false' }},
            autoplay: {{ $autoplay ? 'true' : 'false' }},
            autoplayTimeout: {{ $autoplayTimeout }},
            autoplayHoverPause: {{ $autoplayHoverPause ? 'true' : 'false' }},
            lazyLoad: {{ $lazyload ? 'true': 'false' }},
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })
    });




    //</script>