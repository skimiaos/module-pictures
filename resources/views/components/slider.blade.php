<div class="owl-carousel">
    @foreach($slides as $slide)
        <div class="item">
            @if($lazyload)
                <img class="owl-lazy" data-src="{{$slide->picture['path']}}" alt="{{$slide->picture['description']}}">
            @else
                <img src="{{$slide->picture['path']}}" alt="{{$slide->picture['description']}}">
            @endif
        </div>
    @endforeach


</div>

