@extends('skimia.angular::form.layout')



@block('page.title')
<?php echo (isset($title) ? str_replace(['{{','}}'],['\'+','+\''],addslashes($title)): 'Titre de l\'action' ); ?>
@endoverride



@block('page.icon')
<?php echo (isset($icon) ? $icon : 'os-icon-cancel-3' ); ?>
@endoverride



@block('page.actions')
    @if($form->canAction('create'))
        <a class="waves-effect waves-light btn-flat transparent white-text"
           ng-click="$state.go('{{$form->getStateStart()}}create',$stateParams)">
            <i class="os-icon-plus-5"/> {{ trans('skimia.angular::crud.actions.list.__add') }}
        </a>
    @endif

    @foreach($actions as $id=>$action)
        <a class="waves-effect waves-light btn-flat transparent white-text @if(isset($action['class']) && $action['class']) {{$action['class']}}@endif"
           ng-click="$state.go('{{$action['state']['name']}}'@if(isset($action['state']['params'])), {{$action['state']['params']}} @endif );">
            @if(isset($action['icon']) && $action['icon']) <i class="{{$action['icon']}}"></i> @endif
            {{$action['label']}}
        </a>
    @endforeach


@endoverride



@block('page.content')
<div os-flex="1" ui-tree>
    <div class="row" ui-tree-nodes="" ng-model="entities">
        <div class="col s12 m4 l2" ng-repeat="pic in entities" ui-tree-node ng-include="'nodes_renderer.html'"></div>
    </div>
</div>
@endoverride

@block('page.scripts')


<script type="text/ng-template" id="nodes_renderer.html">
    <div class="card">
        <div class="card-image">
            <img ng-src="@{{pic.picture.path}}">
            <span class="card-title">@{{pic.picture.alt}}</span>
        </div>
        <div class="card-action">
            @if($form->canAction('get'))
                <a ng-click="$state.go('{{ $form->getStateStart() }}get',angular.extend({'entityId':pic.id},$stateParams));">
                    {{ trans('skimia.angular::crud.actions.get.__name-action') }}
                </a>
            @endif
            @foreach($item_actions as $id=>$action)
                <a ng-click="$state.go('{{$action['state']['name']}}'@if(isset($action['state']['params'])), {{$action['state']['params']}} @endif );">
                    {{$action['label']}}
                </a>
            @endforeach
            @if($form->canAction('edit'))
                <a ng-click="$state.go('{{ $form->getStateStart() }}edit',angular.extend({'entityId':pic.id},$stateParams));">
                    {{ trans('skimia.angular::crud.actions.edit.__name-action') }}
                </a>
            @endif

            @if($form->canAction('delete'))
                <a ng-click="remove(pic)">
                    {{ trans('skimia.angular::crud.actions.delete.__name-action') }}
                </a>
            @endif
        </div>
    </div>
</script>
<style>
    .angular-ui-tree-drag {
        position: absolute;
        pointer-events: none;
        z-index: 999;
        opacity: .8;
        min-width: 400px;
    }
    .angular-ui-tree-drag .card{
        min-width: 400px;
    }
</style>


@endoverride

@Controller
//<script>
    $scope.remove = function($entity, $overridde) {

        if(!angular.isDefined($overridde))
            $overridde = false;

        if ($overridde || confirm("Voulez-vous supprimer ??")) { // Clic sur OK

            var $r = {};
            angular.forEach($stateParams,function(value,key){
                $r[':'+key] = value;
            });


            $url = replace('{{ $delete_url }}', angular.extend($r,{':id': $entity.id}));

            $angular_response($scope).delete($url).success(function (data) {
                $scope.entities.splice($scope.entities.indexOf($entity), 1);
            });
        }
    };
//</script>

@EndController