<?php

return [
    'sliders'=>[
        'sliders'=>[
            'list'=>[
                'identifier'=>['label'=>'Identifiant'],
                'description'=>['label'=>'Description'],
            ],
            'fields'=>[
                'identifier'=>['label'=>'Identifiant'],
                'description'=>['label'=>'Description'],
                'data_options'=>[
                    'label'=>'Champs Personalisés',
                ]
            ],
            'data_options'=>[
                'fields'=>[
                    'element'=>['label'=>'Champ personalisé #'],
                ]
            ],
            'element'=>[
                'fields'=>[
                    'key'=>['label'=>'Clef'],
                    'name'=>['label'=>'Label'],
                    'type'=>['label'=>'Type'],
                ]
            ]

        ],
        'slides'=>[
            'list'=>[
                'picture'=>['label'=>'Image'],
                'description'=>['label'=>'Altérnative de l\'image (SEO)'],
            ],
            'fields'=>[
                'picture'=>['label'=>'Image'],
                'metadata'=>['label'=>'Champs Personalisés'],
            ],
            'picture'=>[

                'fields'=>[

                    'path'=>['label'=>'Chemin'],
                    'alt'=>['label'=>'Description'],
                ]
            ],


        ]

    ]
];