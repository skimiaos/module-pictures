<?php

namespace Skimia\Pictures\Data\Models\Slider;

use Eloquent;


class Slider extends Eloquent{

    protected $table = 'pictures_sliders';
    protected $guarded = ['id', 'created_at','updated_at'];

    public function getDataOptionsAttribute($value){
        if(!isset($value) ||empty($value))
            return [];
        return unserialize($value);
    }

    public function setDataOptionsAttribute($options){
        $this->attributes['data_options'] = serialize($options);
    }

    public function slides()
    {
        return $this->hasMany('Skimia\Pictures\Data\Models\Slider\Slide','slider_id');
    }

}