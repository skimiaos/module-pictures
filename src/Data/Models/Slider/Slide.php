<?php

namespace Skimia\Pictures\Data\Models\Slider;

use Eloquent;


class Slide extends Eloquent{

    protected $table = 'pictures_slides';
    protected $guarded = ['id', 'created_at','updated_at'];

    public function getPictureAttribute($value){
        if(!isset($value) ||empty($value))
            return [];
        return unserialize($value);
    }

    public function setPictureAttribute($options){
        $this->attributes['picture'] = serialize($options);
    }

    public function getMetadataAttribute($value){
        if(!isset($value) ||empty($value))
            return [];
        return unserialize($value);
    }

    public function setMetadataAttribute($options){
        $this->attributes['metadata'] = serialize($options);
    }


}