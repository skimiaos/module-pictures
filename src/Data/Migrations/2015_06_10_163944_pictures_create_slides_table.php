<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PicturesCreateSlidesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('pictures_slides')){
            Schema::create('pictures_slides', function(Blueprint $table){
                $table->increments('id');
                $table->unsignedInteger('slider_id');
                $table->text('picture');
                $table->integer('order')->default(6000);
                $table->text('metadata');

                $table->timestamps();

            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pictures_slides');
	}

}
