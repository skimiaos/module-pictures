<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PicturesCreateSlidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if(!Schema::hasTable('pictures_sliders')){
            Schema::create('pictures_sliders', function(Blueprint $table){
                $table->increments('id');
                $table->string('identifier');
                $table->text('data_options');
                $table->text('description');
                $table->timestamps();

            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pictures_sliders');
	}

}
