<?php

namespace Skimia\Pictures\Data\Forms\Slider;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;

use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;

use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;

use Skimia\Angular\Form\CRUD\OptionsInterface;

use Skimia\Pictures\Data\Models\Slider\Slide;
use Skimia\Pictures\Data\Models\Slider\Slider;


class SlidesCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Slide();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.pictures::form.sliders.slides');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $pic = $options->Fields()->makeAssociativeField('picture')
            ->transAll()
            ->setDisplayOrder(1000);
        $pic->Fields()->makeImageField('path')->transAll();
        $pic->Fields()->makeTextField('alt')->transAll();

        $options->Fields()->makeDynamicField('metadata')->transAll()->setDataSelectorClosure(function(){
            $slider_id = $this->getQueryRouteParams()['slider_id'];
            return Slider::find($slider_id,['data_options'])->data_options;
        });


        static::registerOverrideVarEvent('rest-query',function($query,$list_id){
            return $query->where('slider_id',$list_id)->orderBy('order','ASC');
        });

        static::registerCRUDFormEvent('beforeCreateSave',function($entity){
            $entity->slider_id = $this->getQueryRouteParams()['slider_id'];
        });
        $this->registerOverrideVarEvent('rest-list-response',function($list){

            $slider = Slider::find($this->getQueryRouteParams()['slider_id'],['identifier']);
            $list['slider_name'] = $slider->identifier;
            return $list;
        });

        $this->registerOverrideVarEvent('rest-get-create-response',function($list){

            $slider = Slider::find($this->getQueryRouteParams()['slider_id'],['identifier']);
            $list['slider_name'] = $slider->identifier;
            return $list;
        });

    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-picture-3')->setTitle('Liste des images dans votre Galerie "{{slider_name}}"')/*->bindBlade('skimia.pictures::form.action-list-picture',[

        ])*/;
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-picture-3')->setTitle('Ajouter une nouvelle image a votre Galerie "{{slider_name}}"');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-picture-3')->setTitle('Editer votre Image et ses Informations');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);


        $this->listConfiguration->enableDraggableRows();
        $this->listConfiguration->addIdColumn();
        $this->listConfiguration->getNewColumnDefinition('picture')->type(ListCrudColumnConfiguration::_TYPE_PICTURE)->setSelector('picture.path')->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('description')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName()->setSelector('picture.alt');



        /*$options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Image "%identifier%" sauvegardée');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouvelle image "%identifier%" crée');*/





    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'slides';
    }

    /**
     * retourne la liste des paramètres pour la requette
     * @return array
     */
    public function getQueryParams()
    {
        return [
            'slider_id'
        ];
    }


}