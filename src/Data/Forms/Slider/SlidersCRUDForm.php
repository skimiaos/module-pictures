<?php

namespace Skimia\Pictures\Data\Forms\Slider;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\News\Data\Models\Category;
use Skimia\News\Data\Models\Post;
use Skimia\Pictures\Data\Models\Slider\Slider;

class SlidersCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Slider();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.pictures::form.sliders.sliders');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('identifier')
            ->transAll()
            ->setDisplayOrder(1000);

        $options->Fields()->makeTextareaField('description')
            ->transAll()
            ->setDisplayOrder(1000);

        $fields = $options->Fields()->makeRepeatableField('data_options')->transAll()->setDirection();

        $field = $fields->Field()->makeAssociativeField('element')->transAll();

        $field->Fields()->makeTextField('key')->transAll();
        $field->Fields()->makeTextField('name')->transAll();
        $field->Fields()->makeSelectField('type')->setChoices([
            'textarea'=>'Contenu Long',
            'text'=>'Contenu Court',
            'wysiwyg'=>'Contenu Riche (mis en forme)',
            'nullable'=>'-----',
        ])->transAll();




    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-picture-3')->setTitle('Gestion de vos Galeries');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-picture-3')->setTitle('Créer une nouvelle Galerie');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-picture-3')->setTitle('Editer votre Galerie');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);


        $this->listConfiguration->addIdColumn();
        $name = $this->listConfiguration->getNewColumnDefinition('identifier')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $name = $this->listConfiguration->getNewColumnDefinition('description')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();

        $this->listConfiguration->addItemAction('show_slides','Afficher les slides',
            'pictures_manager.slides*list','{slider_id:row.entity.id}',
            'os-icon-picture-3', 'green'
        );

        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Galerie "%identifier%" sauvegardée');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouvelle Galerie "%identifier%" crée');





    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'sliders';
    }

}