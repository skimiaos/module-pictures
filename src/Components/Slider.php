<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\Pictures\Components;

use Illuminate\Support\Collection;
use Skimia\Pages\Components\Component;
use Skimia\Pictures\Data\Models\Slider\Slider as SliderEntity;
class Slider extends Component{

    protected static $systemName = 'slider';

    protected $name = 'Slider';
    protected $description = 'owl.carousel.js';
    protected $icon = 'os-icon-video';

    protected $show_template = 'skimia.pictures::components.slider';


    protected function makeFields(){

        $this->fields = [
            'use_js'=>[
                'type'=>'checkbox',
                'label'=>'Générer le JS ?',
                'default'=>true
            ],
            'slider_name'=>[
                'type'=>'select',
                'label'=>'choix du slider',
                'choicesFct'=> function(){
                    return SliderEntity::lists('identifier','id');
                }
            ],
            'loop'=>[
                'type'=>'checkbox',
                'label'=>'Boucle infinie',
                'default'=>true
            ],
            'nav'=>[
                'type'=>'checkbox',
                'label'=>'Afficher les boutons suivant/precedent',
                'default'=>false
            ],
            'dots'=>[
                'type'=>'checkbox',
                'label'=>'Afficher les bulles de navigation',
                'default'=>true
            ],
            'lazyload'=>[
                'type'=>'checkbox',
                'label'=>'Chargement dynamique des images',
                'default'=>true
            ],

            'autoplay'=>[
                'type'=>'checkbox',
                'label'=>'Lecture automatique',
                'default'=>false
            ],
            'autoplayHoverPause'=>[
                'type'=>'checkbox',
                'label'=>'Pause au survol',
                'default'=>false
            ],
            'autoplayTimeout'=>[
                'type'=>'text',
                'label'=>'Durée d\'affichage de chaque slide (en ms)',
                'default'=>'5000'
            ],
        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        $merged = $this->position->getConfiguration();

        $merged['slides'] = SliderEntity::find($merged['slider_name'])->slides;
        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected $fields  = [

    ];

    public function getStaticJS()
    {
        return file_get_contents(module_assets('skimia.pictures','/owl.carousel/owl.carousel.min.js'));
    }

    public function getDynJS()
    {
        $data = $this->onJavascript();
        if(!(isset($data['use_js']) && $data['use_js'] == false))

            return \View::make('skimia.pictures::components.slider-js',$data)->render();
        return '';
    }

    public function getStaticCSS()
    {
        return file_get_contents(module_assets('skimia.pictures','/owl.carousel/assets/owl.carousel.css'))."\n".
        file_get_contents(module_assets('skimia.pictures','/owl.carousel/assets/owl.theme.default.css'));
    }

    public function getDynCSS()
    {
        return '';
    }
}