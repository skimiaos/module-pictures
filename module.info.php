<?php

return [
    'name'        => 'Skimia Pictures',
    'author'      => 'Skimia Agency',
    'description' => 'Gestion de tout composant image skimia',
    'namespace'   => 'Skimia\\Pictures',
    'require'     => ['skimia.modules','skimia.blade','skimia.form','skimia.angular','skimia.auth','skimia.backend','skimia.pages']
];